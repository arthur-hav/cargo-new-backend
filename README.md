# Cargo new backend

Install command:

```cargo install cargo-new-backend```

Invoke:

```cargo new-backend <path>```

To create a prompt that will automatically fills the `path` with a customized fully working backend with examples.