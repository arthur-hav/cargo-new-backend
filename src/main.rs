use auth_git2::GitAuthenticator;
use git2::{build::RepoBuilder, FetchOptions, ProxyOptions, Repository};
use gix_config::{File as GitConfigParser, Source};
use home;
use std::{env::args, path::Path, path::PathBuf};
use std::fs::remove_dir_all;
use walkdir::{WalkDir, DirEntry};

struct RepoCloneBuilder<'cb> {
    builder: RepoBuilder<'cb>,
    authenticator: GitAuthenticator,
    url: String,
}

/// deals with `~/` and `$HOME/` prefixes
pub fn canonicalize_path(p: impl AsRef<Path>) -> PathBuf {
    let p = p.as_ref();
    let p = if p.starts_with("~/") {
        home::home_dir()
            .unwrap()
            .join(p.strip_prefix("~/").unwrap())
    } else if p.starts_with("$HOME/") {
        home::home_dir()
            .unwrap()
            .join(p.strip_prefix("$HOME/").unwrap())
    } else {
        p.to_path_buf()
    };

    return p.canonicalize().unwrap();
}

pub fn find_gitconfig() -> Option<PathBuf> {
    let gitconfig = home::home_dir()?.join(".gitconfig");
    if gitconfig.exists() {
        return Some(gitconfig);
    }
    None
}

/// trades urls, to replace a given repo remote url with the right on based
/// on the `[url]` section in the `~/.gitconfig`
pub fn resolve_instead_url(remote: impl AsRef<str>, gitconfig: impl AsRef<Path>) -> Option<String> {
    let gitconfig = gitconfig.as_ref();
    let remote = remote.as_ref().to_string();
    let config = GitConfigParser::from_path_no_includes(gitconfig, Source::User).unwrap();
    let x = config.sections_by_name("url").and_then(|iter| {
        iter.map(|section| {
            let head = section.header();
            let body = section.body();
            let url = head.subsection_name();
            let instead_of = body
                .value("insteadOf")
                .map(|x| std::str::from_utf8(&x[..]).unwrap().to_owned());
            (instead_of, url)
        })
        .filter(|(old, new)| new.is_some() && old.is_some())
        .find_map(|(old, new)| {
            let old = old.unwrap();
            let new = new.unwrap().to_string();
            remote
                .starts_with(old.as_str())
                .then(|| remote.replace(old.as_str(), new.as_str()))
        })
    });

    return x;
}

impl<'cb> RepoCloneBuilder<'cb> {
    pub fn new(url: &str) -> Self {
        let url = find_gitconfig().map_or_else(
            || url.to_owned(),
            |gitcfg| resolve_instead_url(url, gitcfg).expect("correct configuration"),
        );

        Self {
            builder: RepoBuilder::new(),
            authenticator: GitAuthenticator::default(),
            url,
        }
    }

    pub fn new_with(url: &str, branch: Option<&str>, identity_path: Option<&Path>) -> Self {
        let mut builder = Self::new(url);
        if let Some(branch) = branch {
            builder.set_branch(branch);
        }

        if let Some(identity_path) = identity_path {
            builder.set_identity(identity_path);
        }

        return builder;
    }

    pub fn set_identity(&mut self, identity_path: &Path) {
        let identity_path = canonicalize_path(identity_path);
        self.authenticator = GitAuthenticator::new_empty()
            .add_ssh_key_from_file(identity_path, None)
            .prompt_ssh_key_password(true);
    }

    pub fn set_branch(&mut self, branch: &str) {
        self.builder.branch(branch);
    }

    fn clone(self, dest_path: &Path) -> Repository {
        let config = git2::Config::open_default().unwrap();

        let mut proxy_options = ProxyOptions::new();
        proxy_options.auto();

        let mut callbacks = git2::RemoteCallbacks::new();
        callbacks.credentials(self.authenticator.credentials(&config));

        let mut fetch_options = FetchOptions::new();
        fetch_options.proxy_options(proxy_options);
        fetch_options.remote_callbacks(callbacks);

        let mut builder = self.builder;
        builder.fetch_options(fetch_options);

        builder.clone(&self.url, dest_path).unwrap()
    }

    pub fn clone_with_submodules(self, dest_path: &Path) -> Repository {
        let authenticator = Clone::clone(&self.authenticator);
        let repo = self.clone(dest_path);

        let config = repo.config().unwrap();

        for mut sub in repo.submodules().unwrap() {
            let mut proxy_options = ProxyOptions::new();
            proxy_options.auto();

            let mut callbacks = git2::RemoteCallbacks::new();
            callbacks.credentials(authenticator.credentials(&config));

            let mut fetch_options = FetchOptions::new();
            fetch_options.proxy_options(proxy_options);
            fetch_options.remote_callbacks(callbacks);

            let mut update_options = git2::SubmoduleUpdateOptions::new();
            update_options.fetch(fetch_options);
            sub.update(true, Some(&mut update_options)).unwrap();
        }

        return repo;
    }
}

fn remove_git(entry: &DirEntry) -> bool {
    let is_git = entry.file_name()
         .to_str()
         .map(|s| (s == ".git" || s == ".github"))
         .unwrap() && entry.file_type().is_dir();
    if is_git{
        remove_dir_all(entry.path()).unwrap();
    }
    is_git
}


// clone git repository into temp using libgit2
fn clone_git_template(git: &str, branch: Option<&str>, path: &str) -> Repository {
    let builder = RepoCloneBuilder::new_with(git, branch, None);

    let repo = builder.clone_with_submodules(Path::new(path));

    let tag = None;
    if let Some(tag) = tag {
        let (object, reference) = repo.revparse_ext(tag).unwrap();
        repo.checkout_tree(&object, None).unwrap();
        reference
            .map_or_else(
                || repo.set_head_detached(object.id()),
                |gref| repo.set_head(gref.name().unwrap()),
            )
            .unwrap()
    }
    return repo
}


fn clean_git(path: &str){
    let mut buf = PathBuf::new();
    buf.push(path);
    let walker = WalkDir::new(path).into_iter();
    for entry in walker.filter_entry(|e| !remove_git(e)) {
        println!("{}", entry.unwrap().path().display());
    }
}

pub fn merge_with(repo: Repository, branch_name: &str){
    let ref_branch = repo
        .find_branch(branch_name, git2::BranchType::Remote)
        .unwrap()
        .into_reference();
    let ac = repo.reference_to_annotated_commit(&ref_branch).unwrap();
    repo.merge(&[&ac], None, None).unwrap();
}

pub fn main() {
    let path = args().nth(1).unwrap();
    let repo = clone_git_template(
        "https://gitlab.com/arthur-hav/new-backend-examples.git",
        Some("main"),
        path.as_str(),
    );
    merge_with(repo, "docker");
    clean_git(path.as_str())
}
